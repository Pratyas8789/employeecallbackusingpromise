const mainFunction = require('../main');
const path = require('path');
const { error } = require('console');


mainFunction.retrieve([2, 13, 23])
    .then((response) => {
        return response;
    })
    .then((response) => {
        return mainFunction.groupDataBasedOnCompanies(response);
    })
    .then((response) => {
        return mainFunction.powerpuffBrigadeCompanyData(response);
    })
    .then((response) => {
        return mainFunction.removeEntry(response, 2);
    })
    .then((response) => {
        return mainFunction.sortData(response);
    })
    .then((response) => {
        return mainFunction.swapPosition(response);
    }).then((response) => {
        return mainFunction.addBirthday(response);
    })
    .then((response) => {
        console.log(response);
    })
    .catch((error) => {
        console.error(error);
    })

