const fs = require('fs');
const path = require('path');

function retrieve(findIds) {
    return new Promise((resolve, reject) => {
        fs.readFile('./data.json', 'utf8', (error, data) => {
            if (error) {
                reject(err);
            }
            else {
                try {
                    data = (JSON.parse(data));
                } catch (error) {
                    reject(error);
                }
                let retrieveData = data["employees"].filter((employeeData) => (findIds).includes(employeeData.id));
                try {
                    retrieveData = JSON.stringify(retrieveData, null, 4);
                } catch (error) {
                    reject(error);
                }
                fs.writeFile(path.join('../output', "retrieveData.json"), retrieveData, (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(data);
                    }
                });
            }
        });
    });
}


function groupDataBasedOnCompanies(data) {
    return new Promise((resolve, reject) => {
        let groupDataBasedOnCompanies = data["employees"].reduce((acc, employeeData) => {
            if (employeeData["company"] === "Scooby Doo") {
                acc["Scooby Doo"].push(employeeData);
            } else if (employeeData["company"] === "Powerpuff Brigade") {
                acc["Powerpuff Brigade"].push(employeeData);
            } else if (employeeData["company"] === "X-Men") {
                acc["X-Men"].push(employeeData);
            }
            return acc;
        }, {
            "Scooby Doo": [],
            "Powerpuff Brigade": [],
            "X-Men": []
        })
        try {
            groupDataBasedOnCompanies = JSON.stringify(groupDataBasedOnCompanies, null, 4);
        } catch (error) {
            reject(error);
        }
        fs.writeFile(path.join('../output', "groupDataBasedOnCompanies.json"), groupDataBasedOnCompanies, (error) => {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    });
}


function powerpuffBrigadeCompanyData(data) {
    return new Promise((resolve, reject) => {
        let powerpuffBrigadeCompanyData = data["employees"].filter((employeeData) => (employeeData.company === "Powerpuff Brigade"))
        try {
            powerpuffBrigadeCompanyData = JSON.stringify(powerpuffBrigadeCompanyData, null, 4);
        } catch (error) {
            reject(error);
        }
        fs.writeFile(path.join('../output', "powerpuffBrigadeCompanyData.json"), powerpuffBrigadeCompanyData, (error) => {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    });
}

function removeEntry(data, id) {
    return new Promise((resolve, reject) => {
        let removeEntry = data["employees"].filter((employeeData) => (employeeData.id !== id));
        try {
            removeEntry = JSON.stringify(removeEntry, null, 4);
        } catch (error) {
            reject(error);
        }
        fs.writeFile(path.join('../output', "removeEntry.json"), removeEntry, (error) => {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    });
}

function sortData(data) {
    return new Promise((resolve, reject) => {
        let sortedData = data["employees"].sort((employee1, employee2) => {
            if (employee1.company === employee2.company) {
                if (employee1.id < employee2.id) {
                    return -1;
                }
            }
            else if (employee1.company < employee2.company) {
                return -1;
            }
        });
        try {
            sortedData = JSON.stringify(sortedData, null, 4);
        } catch (error) {
            reject(error);
        }
        fs.writeFile(path.join('../output', "sortedData.json"), sortedData, (error) => {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    });
}

function swapPosition(data) {
    return new Promise((resolve, reject) => {
        const dataWhichId92 = data["employees"].filter((ele) => ele.id === 92);
        const dataWhichId93 = data["employees"].filter((ele) => ele.id === 93);
        let companyWhichId92 = dataWhichId92[0]["company"];
        let companyWhichId93 = dataWhichId93[0]["company"];
        let newResult = data.employees.map(obj => {
            if (obj.id === 92) {
                obj.company = companyWhichId93;
            }
            if (obj.id === 93) {
                obj.company = companyWhichId92;
            }
            return obj;
        });
        try {
            newResult = JSON.stringify(newResult, null, 4);
        } catch (error) {
            reject(error);
        }
        fs.writeFile(path.join('../output', "swapPosition.json"), newResult, (error) => {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    });
}

function addBirthday(data) {
    return new Promise((resolve, reject) => {
        let addBirthday = data.employees.filter((currentEmployee) => {
            if (currentEmployee.id % 2 === 0) {
                currentEmployee.birthday = new Date().toString().substring(4, 15);
            }
            return currentEmployee;
        });
        try {
            addBirthday = JSON.stringify(addBirthday, null, 4);
        } catch (error) {
            reject(error);
        }
        fs.writeFile(path.join('../output', 'addBirthday.json'), addBirthday, (error) => {
            if (error) {
                reject(error);
            } else {
                resolve("All work done successfully!!!");
            }
        });
    });
}

module.exports.retrieve = retrieve;
module.exports.groupDataBasedOnCompanies = groupDataBasedOnCompanies;
module.exports.powerpuffBrigadeCompanyData = powerpuffBrigadeCompanyData;
module.exports.removeEntry = removeEntry;
module.exports.sortData = sortData;
module.exports.swapPosition = swapPosition;
module.exports.addBirthday = addBirthday;